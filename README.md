# La pilule rouge : EscapeGame pour apprendre la ligne de commande

## A propos
L'objectif de ce jeu est d'enseigner les bases de la ligne de commande en donnant une occasion de répeter les commandes les plus utiles (cd, ls, cat).

## Requirements
Ce jeu a été testé sous linux mint. 

**Espace disque libre : 100G (La fin du jeu lance le programme AgentSmith qu'il faut tuer avnt qu'il ne remplisse le disque)**

## Installation

- Téléchargez le jeu à cette adresse : https://framagit.org/reflexlibre/formations/escapegame/-/archive/master/escapegame-master.tar.gz
- Décompressez-le
- Ouvrir le dossier décompréssé
- Clic-droit "Ouvrir dans un terminal"
- Lancez la commande `./install` en appuyant sur Entrée aprés lk'avoir tappée
- Entrez dans l'école `cd Ecole`
- Le jeu se passe alors dans le terminal

## Commandes qui sont vues dans ce jeu

- `ls` : Regarder ce qu'il y a dans la salle courante (= regardre le contenu du dossier)
- `ls -lh` : Regarder ce qu'il y a dans la salle courante avec plus de détails
- `cd SALLE` : Entrer dans une salle (= entrer dans un dossier)
- `cd ..` : Revenir en arrière
- `cat OBJET/PERSONNE` : Examiner un objet ou écouter une personne (afficher le contenu d'un fichier)
- `chmod +rx SALLE`: Ouvrir une salle fermée
- `chmod +rx PROGRAMME`: Rendre un programme executable
- `./PROGRAMME.sh`: Lancer le programme
- `mkdir NOUVELLE_SALLE`: Créer une salle
- `mv OBJET DESTINATION`: Bouger un objet vers une destination
- `cp OBJET DESTINATION`: Copier un objet vers une destination
- `df -h`: Regarder l'espace disque restant
- `ps aux --forest`: afficher les processus sous forme d'arbre
- `kill NUMERO_PROCESSUS`: tuer un processus
- `rm FICHIER`: supprimer un fichier

## Solutions
Ci-dessous les commandes à taper pour réussir ce jeu:
```
cd Ecole
ls
cd SalleDesProfs
cat MaitreDesClés
cd ..
cd SalleDeChimie
chmod +rx SalleDeChimie
cd SalleDeChimie
cat Morpheus
chmod +rx PiluleRouge.sh
./PiluleRouge.sh
cd ..
cd SousSol
mkdir SalleSecrete
cd ../SalleDeChimie
mv PiluleRouge.sh ../SousSol/SalleSecrete/
cd ../SousSol/SalleSecrete/
./PiluleRouge.sh
cp ../1erEtage/2emeEtage/SalleInfo/Telephone ./
./PiluleRouge.sh
```

Ouvrir un nouveau terminal
```
cd ~/Bureau/escapegame/Ecole
ls
df -h
ps aux --forest
kill NUMERO
rm AgentSmith*
rm .AgentSmith*
```

Gagné

# Auteur
ljf

# License
GPL-3 (sauf ascii art et dialogue de Morpheus)
