#!/bin/bash


# Condition pour activer la pilule
directory="$(pwd)"

if pwd | grep -q '3emeEtage/SalleSecrete'; then
    echo ""
else
cat <<EOF
\e[0;33mLe lapin blanc:\e[0m Attention ne prends pas la pilule ici!
Tu devrais te construire une salle secrète au 3emeEtage avec la commande \e[0;92mmkdir SalleSecrete\e[0m
Puis retourne en \e[0;92mSalleDeChimie\e[0m et \e[0;92mdéplace\e[0m la pilule dans la \e[0;92mSalleSecrete\e[0m avec 
la commande \e[0;92mmv PiluleRouge.sh ../1erEtage/2emeEtage/3emeEtage/SalleSecrete/\e[0m

EOF
	exit
fi

if [ ! -f Telephone ]; then
    cat <<EOF
\e[0;33mLe lapin blanc:\e[0m Pour que la pilule fonctionne il faut un téléphone ici. Problème: tu n es pas le propriétaire du téléphone de la salle informatique, tu vas devoir \e[0;92mcopier\e[0m le fichier Telephone avec \e[0;92mcp ../../../../SousSol/SalleInfo/Telephone Telephone\e[0m

EOF

	exit
fi

# Activation de la pilule, l'agent smith débarque !
cat <<EOF
\e[0;33mMorpheus:\e[0m Bon voyage Alice
      ..______________________________________||__||___________
    .  |                                      |__|||    .____.
   .   |                     .________.       ||__||    |/\ *|
 .     |                     |      .'|       |__|||    |____|
.      |                     |    .'  |     __||__||__.
   /|  |                     |    |   |    /__|__||/_/|
  /%|  |                     |    |   |    |_._____._||
 |@/   |          \e[0;31mo\e[0m          |    |o  |    |_|/^^^\|_||
 |/    |_________\e[0;31m/|l\e[0m__________|____|  ,|____|_|*****|_|/________
      .           \e[0;31m|\e[0m               | ,      ____________      .
    .            \e[0;31m/ l\e[0m              |.     =/ o      o  /=
   .                                    =/    X  .   /=
 .                                     =/___________/=     .
EOF
builtin cd ../../../../
echo "#!/bin/bash" > AgentSmith942.sh
echo "sleep 60" >> AgentSmith942.sh
echo 'NUMERO=$((1 + RANDOM % 900))' >> AgentSmith942.sh
echo 'fallocate -l 100M .AgentSmith$NUMERO' >> AgentSmith942.sh
echo 'cp "$(readlink -f $0)" AgentSmith$NUMERO.sh' >> AgentSmith942.sh
echo './AgentSmith$NUMERO.sh &' >> AgentSmith942.sh
chmod u+x AgentSmith942.sh
./AgentSmith942.sh &

cat <<EOF

\e[0;33mLe lapin blanc:\e[0m On a un problème ! L'\e[0;31magent Smith\e[0m est arrivé dans l'entrée', il est en train de se répliquer, il va envahir toute la matrice !

           \e[0;94mPRENOM:\e[0m Ok on se barre ?

\e[0;33mLe lapin blanc:\e[0m Non, toi tu es l'élu⋅e, tu restes.

           \e[0;94mPRENOM:\e[0m o_O'

\e[0;33mLe lapin blanc:\e[0m Tu peux vérifier combien d'espace il reste dans le \e[1mdisque dur\e[0m de la matrice avec '\e[0;92mdf -h\e[0m' regarde la ligne "Monté sur /" 
Tu peux aussi regarder sur le Bureau il y a un pourcentage d'espace utilisé.

           \e[0;94mPRENOM:\e[0m Super, mais pour les agents Smith qui se répliquent ?

\e[0;33mLe lapin blanc:\e[0m Pour tuer l'\e[0;31magent Smith\e[0m il faut commencer par tuer son \e[1mprocessus\e[0m dans la matrice. Tu peux voir les processus de tous les programmes en cours d execution' avec '\e[0;92mps aux --forest\e[0m'
Une fois que tu as son numéro (dans la 2eme colonne), lance '\e[0;92mkill NUMERO\e[0m' puis tu pourras nettoyer le hall avec '\e[0;92mrm AgentSmith*\e[0m' et '\e[0;92mrm .AgentSmith*\e[0m'
Tu n'as que \e[0;92m20s\e[0m avant que l'agent SMITH change de numéro, il faudra sans doute essayer plusieurs fois.

Une fois que tu as finis tape '\e[0;92mexit\e[0m' pour sortir de la matrice

\e[0;33mLe lapin blanc:\e[0m Bon courage !


EOF


# Fin du jeu

while ls -a | grep -q AgentSmith ; do
    /bin/bash
    ls -a | grep -q AgentSmith && echo "Il en reste encore !!!"
done

cat <<EOF
  ____  _____       __      ______    _ _ _ 
 |  _ \|  __ \     /\ \    / / __ \  | | | |
 | |_) | |__) |   /  \ \  / / |  | | | | | |
 |  _ <|  _  /   / /\ \ \/ /| |  | | | | | |
 | |_) | | \ \  / ____ \  / | |__| | |_|_|_|
 |____/|_|  \_\/_/    \_\/   \____/  (_|_|_)
      
Tu as libéré l'école Matrice des agents Smith
Fin du programme

EOF
cd ../../../../..
./install
